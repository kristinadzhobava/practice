/* Build config:
  ========================================================================== */
  
const webpack = require('webpack')
// Source: https://github.com/survivejs/webpack-merge
const { merge } = require('webpack-merge')

// Base config
const baseWebpackConfig = require('./webpack.base.conf')

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    plugins: [
        // Provides jQuery for other JS bundled with Webpack
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
})

module.exports = new Promise((resolve, reject) => {
    resolve(buildWebpackConfig)
})
