// Bootstrap
import 'bootstrap';

// jQuery
import jQuery from "jquery";
window.jQuery = window.$ = jQuery;

// Swiper
import Swiper from 'swiper';
import 'swiper/swiper.min.css';

// Fancybox
import '@fancyapps/fancybox';
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css';

$(function(){

	var Menu = {
	  
	  el: {
	    ham: $('.menu'),
	    menuTop: $('.menu-top'),
	    menuMiddle: $('.menu-middle'),
	    menuBottom: $('.menu-bottom')
	  },
	  
	  init: function() {
	    Menu.bindUIactions();
	  },
	  
	  bindUIactions: function() {
	    Menu.el.ham
	        .on(
	          'click',
	        function(event) {
	        Menu.activateMenu(event);
	        event.preventDefault();
	      }
	    );
	  },
	  
	  activateMenu: function() {
	    Menu.el.menuTop.toggleClass('menu-top-click');
	    Menu.el.menuMiddle.toggleClass('menu-middle-click');
	    Menu.el.menuBottom.toggleClass('menu-bottom-click'); 
	  }
	};

	Menu.init();

	$('.menu').click(function(){
		var menu_mobile = $('.menu-mobile');
		if(menu_mobile.hasClass('active')){
			menu_mobile.removeClass('active');
		} else {
			menu_mobile.addClass('active');
		}
		
	});

});



